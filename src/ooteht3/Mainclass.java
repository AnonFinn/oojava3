/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ooteht3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;
import java.util.ArrayList;
/**
 *
 * @author f0387852
 */
public class Mainclass {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        // TODO code application logic here
        BottleDispenser automaatti = new BottleDispenser();
        Boolean totuus = true;
        int valinta, ostos;
        String line;
        
        while (totuus) {
            System.out.println();
            System.out.println("*** LIMSA-AUTOMAATTI ***");
            System.out.println("1) Lisää rahaa koneeseen");
            System.out.println("2) Osta pullo");
            System.out.println("3) Ota rahat ulos");
            System.out.println("4) Listaa koneessa olevat pullot");
            System.out.println("0) Lopeta");
            System.out.print("Valintasi: ");
            
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            line = br.readLine();
            valinta = Integer.parseInt(line);
            
            switch (valinta) {
                
                case 1:
                    automaatti.addMoney();
                    break;
                case 2:
                    automaatti.listBottles();
                    System.out.print("Valintasi: ");
                    line = br.readLine();
                    ostos = Integer.parseInt(line);
                    automaatti.buyBottle(ostos);
                    break;
                case 3:
                    automaatti.returnMoney();
                    break;
                case 4:
                    automaatti.listBottles();
                    break;
                case 0:
                    totuus = false;
                    break;
                default:
                    System.out.println("Huono valinta");
                    break;
                
            }
        }
        
    }
    
}
