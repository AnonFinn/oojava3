/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ooteht3;

/**
 *
 * @author f0387852
 */
public class Bottle {
    public String name;
    public double size;
    public double price;
    
    
    public Bottle() {
        name = "Pepsi Max";
        size = 0.5;
        price = 1.8;
        
    }
    public Bottle(String n, double s, double p) {
        name = n;
        size = s;
        price = p;
    }
}
