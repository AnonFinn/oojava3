/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ooteht3;


import java.util.ArrayList;
/**
 *
 * @author f0387852
 */
public class BottleDispenser {
    
    private int bottles;
    // The array for the Bottle-objects
    private ArrayList<Bottle> bottle_array = new ArrayList();
    private double money;
    
    public BottleDispenser() {
        bottles = 6;
        money = 0.0;
        
        
        bottle_array.add(new Bottle("Pepsi Max", 0.5, 1.8));
        bottle_array.add(new Bottle("Pepsi Max", 1.5, 2.2));
        bottle_array.add(new Bottle("Coca-Cola Zero", 0.5, 2.0));
        bottle_array.add(new Bottle("Coca-Cola Zero", 1.5, 2.5));
        bottle_array.add(new Bottle("Fanta Zero", 0.5, 1.95));
        bottle_array.add(new Bottle("Fanta Zero", 0.5, 1.95));
        
        // Initialize the array
        // Add Bottle-objects to the array
        //for(int i = 0;i<bottles;i++) {
            // Use the default constructor to create new Bottles
           // bottle_array.add(new Bottle());
        //}
    }
    
    public void addMoney() {
        money += 1.0;
        System.out.println("Klink! Lisää rahaa laitteeseen!");
    }
    
    public void buyBottle(int valinta) {
        if (bottles > 0 && money >= bottle_array.get(valinta-1).price) {
            bottles -= 1;
            money -= bottle_array.get(valinta-1).price;
            System.out.println("KACHUNK! " + bottle_array.get(valinta-1).name + " tipahti masiinasta!");
            removeBottle(valinta);
        }else if (bottles == 0) {
            System.out.println("Automaatti tyhjä!");
        }else {
            System.out.println("Syötä rahaa ensin!");
        }
    }
    
    public void returnMoney() {
        
        System.out.format("Klink klink. Sinne menivät rahat! Rahaa tuli ulos %.2f€%n", money);
        money = 0;
    }
    
    private void removeBottle(int valinta) {
        bottle_array.remove(valinta - 1);
    }
    
    public void listBottles() {
        for (int i = 0; i < bottle_array.size(); i++ ) {
            System.out.println(i+1 + ". Nimi: " + bottle_array.get(i).name);
            System.out.println("\tKoko: " + bottle_array.get(i).size + "\tHinta: " + bottle_array.get(i).price);
        }
    }
}
